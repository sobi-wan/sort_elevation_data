###########################################################################
# IMPORTANT: this script works best if put in the same folder as Ortho4XP #
# main directory. It was also tested executed inside Elevarion_dir        #
# directory, but few lines needs to be changed in order to run it from    #
# there without errors.                                                   #
#                                                                         #
# Usage: execute script, no extra parameters required.                    #
#                                                                         #
# Extract archive/archives downloaded from http://viewfinderpanoramas.org #
# into Elevation_data directory and execute this script.                  #
# Script performs two loops: first - it creates required subdirectories,  #
# basing on filenames recursively found in  Elevation_data directory.     #
# Second loop moves these files into apropriate locations.                #
# At the end, scripts removes all empty directories from Elevation_data.  #
###########################################################################

import os
import pathlib
import sys
from math import floor

Ortho4XP_dir  =  '..' if getattr(sys,'frozen',False) else '.'
Elevation_dir =  os.path.join(Ortho4XP_dir, 'Elevation_data')

if not os.path.exists("Ortho4XP.cfg"):
	input('Ortho4XP was not detected. Check if this script is located in Ortho4XP\'s toot directory.')
	exit()

##############################################################################

def dst_dirname(hemisphere, latitude, greenwichside, longitude):
    hemisphere_prefix='+' if hemisphere == 'N' else '-'
    greenwichside_prefix='+' if greenwichside == 'E' else '-'
    latitude_suffix='{:.0f}'.format(floor(int(latitude)/10)*10).zfill(2)
    longitude_suffix='{:.0f}'.format(floor(int(longitude)/10)*10).zfill(3)
    return hemisphere_prefix+latitude_suffix+greenwichside_prefix+longitude_suffix
    
##############################################################################

filelist = pathlib.Path(Elevation_dir).glob('**/*.hgt')
for filenamext in filelist:
  try:
    filename = os.path.splitext(os.path.basename(filenamext))[0]
    hemisphere, latitude, greenwichside, longitude = filename[0:1], filename[1:3], filename[3:4], filename[4:7]
    dest_directory_name = dst_dirname(hemisphere, latitude, greenwichside, longitude)
    path = pathlib.Path(os.path.join(Elevation_dir, dest_directory_name))
    path.mkdir(parents=True, exist_ok=True)
  except OSError:
    print ("[E] MKDIR: %s" % path)

filelist = pathlib.Path(Elevation_dir).glob('**/*.hgt')
for filenamext in filelist:
  try:
    filename = os.path.splitext(os.path.basename(filenamext))[0]
    hemisphere, latitude, greenwichside, longitude = filename[0:1], filename[1:3], filename[3:4], filename[4:7]
    dest_directory_name = dst_dirname(hemisphere, latitude, greenwichside, longitude)
    os.rename(filenamext, os.path.join(Elevation_dir, dest_directory_name, os.path.basename(filenamext)))
  except OSError:
    print ("[E] MOVED: %s -> %s" % (filenamext, os.path.join(Elevation_dir, dest_directory_name, os.path.basename(filenamext))))
  else:
    print ("[I] MOVED: %s -> %s" % (filenamext, os.path.join(Elevation_dir, dest_directory_name, os.path.basename(filenamext))))

def removeEmptyFolders(path):
  if not os.path.isdir(path):
    return

  files = os.listdir(path)
  if len(files):
    for f in files:
      fullpath = os.path.join(path, f)
      if os.path.isdir(fullpath):
        removeEmptyFolders(fullpath)

  files = os.listdir(path)
  if len(files) == 0:
    try:
      os.rmdir(path)
    except OSError:
      print("[E] RMDIR: %s:" % path)

removeEmptyFolders(Elevation_dir)
